import Mongoose from 'mongoose';
import config from 'config';

const dbConfig = config.get('Customer.dbConfig');

Mongoose.connect(dbConfig.host + "/" + dbConfig.dbName, dbConfig.properties);

const db = Mongoose.connection;
db.on('error', console.error.bind(console, 'Connection error.'));
db.once('open', function callback() {
  console.log("Connection with database succeeded.");
});

exports.db = db;
