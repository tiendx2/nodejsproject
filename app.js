import express from 'express';
import config from 'config';
import db from './db/db';

const app = express();

const dbConfig = config.get('Customer.dbConfig');
app.listen(dbConfig.port, () => {
    console.log("App listening on port " + dbConfig.port);
})

module.exports = app;